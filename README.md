This program I created for ODIN. It's a Caesar cipher which rotates alphabetical characters, and accepts any other characters and returns those as part of the returned string.
I'm pretty proud of it, as I created it almost entirely before running it once, and didn't look at any other code to get help.
